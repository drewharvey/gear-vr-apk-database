define([
  'backbone'
], function() {

  var Apk = Bacbone.Model.extend({
    defaults: {
      id: null,
      name: "",
      filename: "",
      rating: "NA",
      downloads: 0,
      author: ""
    },
    url: function() {
      return "apks/" + this.get('id');
    }
  });
  
  return Apk;
});