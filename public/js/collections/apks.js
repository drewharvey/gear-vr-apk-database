define([
  'backbone',
  'models/apk'
], function(Backbone, Apk) {
   
    var ApkCollection = Backbone.Collection.extend({
      model: Apk,
      url: function() {
        return '/apks';
      }
    });
   
    return ApkCollection; 
});