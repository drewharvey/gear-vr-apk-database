define([
  'backbone',
  'views/home'
], function(Backbone, Home) {
  var start = function() {
    var home = new Home();
    home.render();
  };
  return {start: start};
});