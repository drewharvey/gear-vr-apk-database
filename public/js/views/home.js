define([
  'backbone',
  'handlebars'
], function() {
  var Home = Backbone.View.extend({
    el: '#content',
    initialize: function() {

    },
    render: function() {
      this.$el.html("<h1>Gear VR APK Database</h1>");
    }
  });
  return Home;
});