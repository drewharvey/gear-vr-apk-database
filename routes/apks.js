var express = require('express');
var router = express.Router();

router.route('/')
  .get(function(req, res, next) {
    res.send("get apks");
  })
  .post(function(req, res, next) {

  });

router.route('/:id')
  .get(function(req, res, next) {
    var id = req.params.id;
    res.send("ID: " + id);
  })
  .post(function(req, res, next) {
    
  });

module.exports = router;